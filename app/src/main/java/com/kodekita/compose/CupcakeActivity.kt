package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.kodekita.compose.ui.screen.CupcakeApp
import com.kodekita.compose.ui.theme.ComposeJuaraTheme

class CupcakeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                CupcakeApp()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CupcakeActivityReview() {
    ComposeJuaraTheme {
        CupcakeApp()
    }
}