package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.kodekita.compose.ui.screen.GameScreen
import com.kodekita.compose.ui.theme.ComposeJuaraTheme

class UnscrambleActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                GameScreen()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun UnscrambleAppsPreview() {
    ComposeJuaraTheme {
        GameScreen()
    }
}