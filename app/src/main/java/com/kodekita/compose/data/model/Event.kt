package com.kodekita.compose.data.model

data class Event(
    val title: String,
    val description: String? = null,
    val daypart: Daypart,
    val durationInMinutes: Int,
) {
    fun durationOfEvent(): String = if (this.durationInMinutes < 60) {
        "short"
    } else {
        "long"
    }
}