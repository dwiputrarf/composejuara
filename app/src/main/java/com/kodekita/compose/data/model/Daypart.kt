package com.kodekita.compose.data.model

enum class Daypart {
    MORNING,
    AFTERNOON,
    EVENING
}