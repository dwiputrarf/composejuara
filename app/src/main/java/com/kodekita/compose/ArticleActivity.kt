package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.kodekita.compose.ui.theme.ComposeJuaraTheme

class ArticleActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val image = painterResource(id = R.drawable.bg_compose_background)
                    Column {
                        Image(
                            painter = image,
                            contentDescription = null,
                            modifier = Modifier.fillMaxWidth()
                        )
                        TextTitle(
                            getString(R.string.article_title),
                            modifier = Modifier
                                .padding(16.dp)
                                .fillMaxWidth()
                        )
                        TextMessage(
                            getString(R.string.article_desc_first),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 16.dp, end = 16.dp)
                        )
                        TextMessage(
                            "In this tutorial, you build a simple UI component with declarative functions. You call Compose functions to say what elements you want and the Compose compiler does the rest. Compose is built around Composable functions. These functions let you define your app\'s UI programmatically because they let you describe how it should look and provide data dependencies, rather than focus on the process of the UI\'s construction, such as initializing an element and then attaching it to a parent. To create a Composable function, you add the @Composable annotation to the function name.",
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(16.dp)
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun TextTitle(message: String, modifier: Modifier = Modifier) {
    Text(text = message, fontSize = 24.sp, modifier = modifier)
}

@Composable
fun TextMessage(message: String, modifier: Modifier = Modifier) {
    Text(text = message, textAlign = TextAlign.Justify, modifier = modifier)
}

@Preview(showBackground = true)
@Composable
fun ArticlePreview() {
    ComposeJuaraTheme {
        TextMessage("Android")
    }
}