package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Email
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material.icons.rounded.Share
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.kodekita.compose.ui.theme.ComposeJuaraTheme

class BusinessCardActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color(0xFFD2E8D4)
                ) {
                    SetStatusBarColor(Color(0xFFD2E8D4))
                    BusinessCard(
                        "Jennifer Doe",
                        "Android Developer Extraordinaire",
                        "+11 (123) 444 555 666",
                        "@AndroidDev",
                        "jen.doe@Android.com"
                    )
                }
            }
        }
    }
}

@Composable
fun BusinessCard(name: String, occupation: String, phone: String, share: String, email: String) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom,
    ) {
        UserInfo(
            color = 0xFF073042,
            name = name,
            occupation = occupation,
            modifier = Modifier
                .weight(1f)
                .fillMaxSize()
        )
        ContactInfo(
            phone, share, email, modifier = Modifier
                .weight(1f)
                .fillMaxSize()
        )
    }
}

@Composable
fun UserInfo(color: Long, name: String, occupation: String, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom
    ) {
        Card(backgroundColor = Color(color)) {
            val image = painterResource(id = R.drawable.android_logo)
            Image(
                painter = image,
                contentDescription = null,
                modifier = Modifier
                    .padding(8.dp)
                    .size(100.dp, 100.dp)
            )
        }
        Text(
            text = name,
            fontSize = 24.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
        Text(
            text = occupation,
            color = Color(0xFF3ddc84),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
fun ContactInfo(phone: String, share: String, email: String, modifier: Modifier = Modifier) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom,
        modifier = modifier
    ) {
        Contact(phone = phone, share = share, email = email)
        Spacer(modifier = Modifier.height(24.dp))
    }
}

@Composable
fun Contact(phone: String, share: String, email: String, modifier: Modifier = Modifier) {
    Column(horizontalAlignment = Alignment.Start, modifier = modifier) {
        IconTextContact(Icons.Rounded.Phone, phone)
        IconTextContact(Icons.Rounded.Share, share)
        IconTextContact(Icons.Rounded.Email, email)
    }
}

@Composable
fun IconTextContact(icon: ImageVector, label: String) {
    Row(
        modifier = Modifier
            .padding(0.dp, 8.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(icon, contentDescription = null, tint = Color(0xFF3ddc84))
        Spacer(modifier = Modifier.height(8.dp))
        Text(text = label, color = Color(0xFF3ddc84))
    }
}

@Composable
fun SetStatusBarColor(color: Color) {
    val systemUiController = rememberSystemUiController()
    SideEffect {
        systemUiController.setSystemBarsColor(color)
    }
}

@Preview(showBackground = true)
@Composable
fun BusinessCardPreview() {
    ComposeJuaraTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = Color(0xFFD2E8D4)
        ) {
            BusinessCard(
                "Jennifer Doe",
                "Android Developer Extraordinaire",
                "+11 (123) 444 555 666",
                "@AndroidDev",
                "jen.doe@Android.com"
            )
        }
    }
}