package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.kodekita.compose.ui.theme.ComposeJuaraTheme

class TaskActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val image = painterResource(id = R.drawable.ic_task_completed)
                    Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                        Image(
                            painter = image,
                            contentDescription = null,
                            alignment = Alignment.Center,
                            modifier = Modifier.size(100.dp, 100.dp)
                        )
                        Text(
                            getString(R.string.task_complete),
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier.padding(0.dp, 8.dp)
                        )
                        Text(getString(R.string.nice_work), fontSize = 16.sp)
                    }
                }
            }
        }
    }
}

@Composable
fun TextMessage(message: String) {
    Text(text = message)
}

@Preview(showBackground = true)
@Composable
fun TaskPreview() {
    ComposeJuaraTheme {
        TextMessage("Android")
    }
}