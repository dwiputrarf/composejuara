package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.kodekita.compose.data.DataSource
import com.kodekita.compose.data.model.Topic
import com.kodekita.compose.ui.theme.ComposeJuaraTheme
import com.kodekita.compose.ui.theme.Shapes

class TopicsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                TopicsApps()
            }
        }
    }
}

@Composable
fun TopicsApps() {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.background
    ) {
        TopicList(DataSource.topics)
    }
}

@Composable
fun TopicList(topics: List<Topic>, modifier: Modifier = Modifier) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        contentPadding = PaddingValues(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalArrangement = Arrangement.spacedBy(4.dp),
        content = {
            items(topics) { topic ->
                TopicsCard(topics = topic)
            }
        })
}

@Composable
fun TopicsCard(topics: Topic, modifier: Modifier = Modifier) {
    Card(shape = Shapes.medium, content = {
        Row {
            Image(
                painter = painterResource(id = topics.image),
                contentDescription = null,
                modifier = Modifier.size(68.dp, 68.dp),
                contentScale = ContentScale.Crop
            )
            Column(
                horizontalAlignment = Alignment.Start,
                modifier = Modifier.padding(0.dp, 16.dp, 16.dp, 0.dp)
            ) {
                Text(
                    text = stringResource(id = topics.title),
                    modifier = Modifier.padding(16.dp, 0.dp, 0.dp, 8.dp)
                )
                LabelTopics(label = topics.count.toString())
            }
        }
    })
}

@Composable
fun LabelTopics(label: String, modifier: Modifier = Modifier) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Icon(
            painter = painterResource(id = R.drawable.ic_grain),
            contentDescription = null,
            modifier = Modifier.padding(16.dp, 0.dp, 8.dp, 0.dp),
            tint = Color.Black
        )
        Text(text = label, modifier = Modifier.padding(0.dp, 0.dp, 16.dp, 0.dp))
    }
}

@Preview(showBackground = true)
@Composable
fun TopicsPreview() {
    ComposeJuaraTheme {
        TopicsApps()
    }
}