package com.kodekita.compose.ui.state

import androidx.annotation.DrawableRes
import com.kodekita.compose.data.DataSource.dessertList

data class DessertClickerState (
    val dessertsSold: Int = 0,
    val revenue: Int = 0,
    val currentDessertIndex: Int = 0,
    val currentDessertPrice: Int = dessertList[currentDessertIndex].price,
    @DrawableRes val currentDessertImageId: Int = dessertList[currentDessertIndex].imageId
)