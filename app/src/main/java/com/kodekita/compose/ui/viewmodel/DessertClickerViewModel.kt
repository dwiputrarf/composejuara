package com.kodekita.compose.ui.viewmodel

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import com.kodekita.compose.R
import com.kodekita.compose.data.model.Dessert
import com.kodekita.compose.ui.state.DessertClickerState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class DessertClickerViewModel: ViewModel() {
    // Dessert UI state
    private val _uiState = MutableStateFlow(DessertClickerState())
    val uiState: StateFlow<DessertClickerState> = _uiState.asStateFlow()

    fun determineDessertToShow(
        desserts: List<Dessert>,
        dessertsSold: Int
    ): Dessert {
        var dessertToShow = desserts.first()
        for (dessert in desserts) {
            if (dessertsSold >= dessert.startProductionAmount) {
                dessertToShow = dessert
            } else {
                break
            }
        }
        return dessertToShow
    }

    fun updateRevenue() {
        _uiState.update { currentState ->
            currentState.copy(
                dessertsSold = currentState.dessertsSold.inc(),
                revenue = currentState.revenue + currentState.currentDessertPrice
            )
        }
    }

    fun showNextDessert(dessert: Dessert) {
        _uiState.update { currentState ->
            currentState.copy(
                currentDessertImageId = dessert.imageId,
                currentDessertPrice = dessert.price
            )
        }
    }

    fun shareSoldDessertsInformation(intentContext: Context, dessertsSold: Int, revenue: Int) {
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(
                Intent.EXTRA_TEXT,
                intentContext.getString(R.string.share_text, dessertsSold, revenue)
            )
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)

        try {
            ContextCompat.startActivity(intentContext, shareIntent, null)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                intentContext,
                intentContext.getString(R.string.sharing_not_available),
                Toast.LENGTH_LONG
            ).show()
        }
    }
}