package com.kodekita.compose.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val md_theme_dark_secondaryContainer = Color(0xFF354A53)
val md_theme_dark_onSecondaryContainer = Color(0xFFCFE6F1)