package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.kodekita.compose.ui.theme.ComposeJuaraTheme

class QuadrantActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MainScreen()
                }
            }
        }
    }
}

@Composable
fun MainScreen() {
    Column(modifier = Modifier.fillMaxSize()) {
        Row(
            modifier = Modifier.weight(1f),
            verticalAlignment = Alignment.CenterVertically
        ) {
            CardQuadrant(
                title = "Text composable",
                desc = "Displays text and follows the recommended Material Design guidelines",
                color = 0xFFEADDFF,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            )
            CardQuadrant(
                title = "Image composable",
                desc = "Creates a composable that lays out and draws a given Painter class object.",
                color = 0xFFD0BCFF,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            )
        }
        Row(
            modifier = Modifier.weight(1f),
            verticalAlignment = Alignment.CenterVertically
        ) {
            CardQuadrant(
                title = "Row composable",
                desc = "A layout composable that places its children in a horizontal sequence.",
                color = 0xFFB69DF8,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            )
            CardQuadrant(
                title = "Column composable",
                desc = "A layout composable that places its children in a vertical sequence.",
                color = 0xFFF6EDFF,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            )
        }
    }
}

@Composable
fun CardQuadrant(title: String, desc: String, color: Long, modifier: Modifier = Modifier) {
    Card(
        backgroundColor = Color(color),
        modifier = modifier
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = title,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(0.dp, 0.dp, 0.dp, 16.dp)
            )
            Text(
                text = desc,
                textAlign = TextAlign.Justify
            )
        }
    }
}

@Preview(showBackground = false)
@Composable
fun QuadrantPreview() {
    ComposeJuaraTheme {
        MainScreen()
    }
}