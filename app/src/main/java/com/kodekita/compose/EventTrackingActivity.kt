package com.kodekita.compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.kodekita.compose.data.model.Daypart
import com.kodekita.compose.data.model.Event
import com.kodekita.compose.ui.theme.ComposeJuaraTheme

class EventTrackingActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                EventTrackingApps()
            }
        }
    }
}

@Composable
fun EventTrackingApps() {
    val event1 = Event(title = "Wake up", description = "Time to get up", daypart = Daypart.MORNING, durationInMinutes = 0)
    val event2 = Event(title = "Eat breakfast", daypart = Daypart.MORNING, durationInMinutes = 15)
    val event3 = Event(title = "Learn about Kotlin", daypart = Daypart.AFTERNOON, durationInMinutes = 30)
    val event4 = Event(title = "Practice Compose", daypart = Daypart.AFTERNOON, durationInMinutes = 60)
    val event5 = Event(title = "Watch latest DevBytes video", daypart = Daypart.AFTERNOON, durationInMinutes = 10)
    val event6 = Event(title = "Check out latest Android Jetpack library", daypart = Daypart.EVENING, durationInMinutes = 45)

    val events = mutableListOf<Event>(event1, event2, event3, event4, event5, event6)
    val shortEvents = events.filter { it.durationInMinutes < 60 }

    val groupedEvents = events.groupBy { it.daypart }

    val durationOfEvent = if (events[0].durationInMinutes < 60) {
        "short"
    } else {
        "long"
    }
    println("Duration of first event of the day: $durationOfEvent")

    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.background
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            Text(text = "You have ${shortEvents.size} short events.")
            Spacer(modifier = Modifier.height(8.dp))
            groupedEvents.forEach { (daypart, events) ->
                Text(text = "$daypart: ${events.size} events")
            }
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = "Last event of the day: ${events.last().title}")
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = "Duration of first event of the day: ${events[0].durationOfEvent()}")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun EventTrackingPreview() {
    ComposeJuaraTheme {
        EventTrackingApps()
    }
}