package com.kodekita.compose

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.kodekita.compose.ui.theme.ComposeJuaraTheme
import com.kodekita.compose.ui.theme.Shapes

class LemonadeActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeJuaraTheme {
                SetStatusBarColor(Color(0xFFF9E44C))
                Scaffold(topBar = {
                    TopAppBar(backgroundColor = Color(0xFFF9E44C), title = {
                        Text(text = "Lemonade", textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
                    })
                }, content = { LemonadeApps() })
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun LemonadeApps() {
    var step by remember { mutableStateOf(1) }
    var squash by remember { mutableStateOf(2) }
    val image = when (step) {
        1 -> R.drawable.lemon_tree
        2 -> R.drawable.lemon_squeeze
        3 -> R.drawable.lemon_drink
        4 -> R.drawable.lemon_restart
        else -> R.drawable.lemon_restart
    }
    val desc = when (step) {
        1 -> stringResource(id = R.string.lemon_tree_desc)
        2 -> stringResource(id = R.string.lemon_desc)
        3 -> stringResource(id = R.string.lemonade_desc)
        4 -> stringResource(id = R.string.empty_glass_desc)
        else -> stringResource(id = R.string.empty_glass_desc)
    }
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.background
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Card(backgroundColor = Color(0xFFC3ECD2), shape = Shapes.large, onClick = {
                when (step) {
                    1 -> {
                        squash = (2..4).random()
                        step++
                    }
                    2 -> {
                        if (squash > 0) {
                            squash--
                        } else {
                            step++
                        }
                    }
                    3 -> step++
                    4 -> step = 1
                }
            }) {
                Image(
                    painter = painterResource(id = image),
                    contentDescription = desc,
                    modifier = Modifier.clip(Shapes.large),
                    contentScale = ContentScale.Crop
                )
            }
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = desc)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LemonadePreview() {
    ComposeJuaraTheme {
        LemonadeApps()
    }
}